
local function DownloadFile(site, file)
	DownloadFileAsync(site, file, function() end)
	local timer = os.clock()
	while os.clock() < timer + 1 do end
	while not FileExist(file) do end
end

local function ReadFile(file)
	local txt = io.open(file, "r")
	local result = txt:read()
	txt:close(); return result
end

local Version = 1.03
local function AutoUpdate()
	DownloadFile("https://raw.githubusercontent.com/samuelchow1997/gos/master/14Series.version", SCRIPT_PATH .. "14Series.version")
	if tonumber(ReadFile(SCRIPT_PATH .. "14Series.version")) > Version then
		print("14Series: Found update! Downloading...")
		DownloadFile("https://raw.githubusercontent.com/samuelchow1997/gos/master/14Series.lua", SCRIPT_PATH .. "14Series.lua")
		print("14Series: Successfully updated.  2x F6")
	end
end

require 'GGPrediction'

local coreMenu = nil
local orbwalker = nil
local prediction = nil

local cachedEnemyHeroes = {}
local cachedAllyHeroes = {}

local table_insert = table.insert
local abs = math.abs 
local sqrt = math.sqrt 
local deg = math.deg 
local acos = math.acos 
local cos = math.cos
local sin = math.sin

local delayedActions, delayedActionsExecuter = {}, nil
local function MyDelayAction(func, delay, args) --delay in seconds
  if not delayedActionsExecuter then
    function delayedActionsExecuter()
      for t, funcs in pairs(delayedActions) do
        if t <= os.clock() then
          for i = 1, #funcs do
            local f = funcs[i]
            if f and f.func then
              f.func(unpack(f.args or {}))
            end
          end
          delayedActions[t] = nil
        end
      end
    end
    -- AddEvent(Events.OnTick, delayedActionsExecuter)
    Callback.Add("Tick", delayedActionsExecuter)

  end
  local t = os.clock() + (delay or 0)
  if delayedActions[t] then
    delayedActions[t][#delayedActions[t] + 1] = {func = func, args = args}
  else
    delayedActions[t] = {{func = func, args = args}}
  end
end

local function GetDistanceSquared(vec1, vec2)
    local dx = vec1.x - vec2.x
    local dy = (vec1.z or vec1.y) - (vec2.z or vec2.y)
    return dx * dx + dy * dy
end

local function GetDistance(a,b)
    return a:DistanceTo(b)
end

local function CircleCircleIntersection(c1, c2, r1, r2)
    local D = GetDistance(c1,c2)
    if D > r1 + r2 or D <= abs(r1 - r2) then return nil end
    local A = (r1 * r2 - r2 * r1 + D * D) / (2 * D)
    local H = sqrt(r1 * r1 - A * A)
    local Direction = (c2 - c1):Normalized()
    local PA = c1 + A * Direction
    local S1 = PA + H * Direction:Perpendicular()
    local S2 = PA - H * Direction:Perpendicular()
    return S1, S2
end

local function Rotate2D(center,rota,rad)            --Credit to Ark223
    local a = rota.x - center.x
    local b = rota.z - center.z

    local c = a*cos(rad)-b*sin(rad)
    local d = a*sin(rad)+b*cos(rad)

    return Vector(c+center.x,rota.y,d+center.z)
end

local function AngleBetween(v3, v1, v2)

    local p1, p2 = (-v3 + v1), (-v3 + v2)
    local theta = p1:Polar() - p2:Polar()
    if theta < 0 then theta = theta + 360 end
    if theta > 180 then theta = 360 - theta end
    return theta
end


-- local function CastSpell(key, position)
--     local cPos = cursorPos
--     local pos1 = position.pos or position
--     local pos = pos1:To2D()

--     Control.SetCursorPos(pos.x, pos.y)
--     MyDelayAction(function() 
--         print("pos "..pos.x.." "..pos.y)
--         print("cursor "..cursorPos.x.." "..cursorPos.y)

--         local dx = cursorPos.x - pos.x
--         local dy = (cursorPos.y) - pos.y
--         local dr = dx * dx + dy *dy

--         print("result "..dr)
--         if dr < 1000 then
--             Control.KeyDown(key)
--             Control.KeyUp(key)
--         end

--         MyDelayAction(function()
--             print(Control.SetCursorPos(cPos.x, cPos.y))
--         end,0.01)

--     end,0.011)


--     -- Control.SetCursorPos(cPos.x, cPos.y)

--     return true
-- end

local function Hasbuff(unit, buffName)
    local buffCount = unit.buffCount
    if buffCount == nil or buffCount < 0 or buffCount > 100000 then
        print("buff api error: buffCount = "..buffCount)
    	return nil
	end

    for i = 0, buffCount do
		local buff = unit:GetBuff(i)
        if buff and buff.count > 0 and buff.name == buffName then 
            return buff
		end
	end
end




local function IsValid(unit)
    return  unit.valid and unit.isTargetable and unit.visible and unit.health > 0
end


local function Ready(spell)
    return Game.CanUseSpell(spell) == 0
end

local function GetEnemyHeroes()
	if #cachedEnemyHeroes == 0 then
		local count = Game.HeroCount()
		if count == nil or count < 0 or count > 10000 then
			print('game.herocount=' .. tostring(count))
			return cachedEnemyHeroes
		end
	    for i = 1, count do
	        local obj = Game.Hero(i)
	        if obj.isEnemy then
	        	table_insert(cachedEnemyHeroes, obj)
        	end
	    end
	end
	return cachedEnemyHeroes
end

local function GetAllyHeroes()
	if #cachedAllyHeroes == 0 then
		local count = Game.HeroCount()
		if count == nil or count < 0 or count > 10000 then
			print('game.herocount=' .. tostring(count))
			return cachedAllyHeroes
		end
	    for i = 1, count do
	        local obj = Game.Hero(i)
	        if obj.isAlly then
	        	table_insert(cachedAllyHeroes, obj)
        	end
	    end
	end
	return cachedAllyHeroes
end

local function OnAllyHeroLoad(cb)
    for i = 1, #GetAllyHeroes() do
        local obj = GetAllyHeroes()[i]
        cb(obj)
    end
end

local function OnEnemyHeroLoad(cb)
    for i = 1, #GetEnemyHeroes() do
        local obj = GetEnemyHeroes()[i]
        cb(obj)
    end
end

local function GetEnemyCountInRange(range, position)
    local position = position or myHero.pos
    local count = 0

    for i = 1, #GetEnemyHeroes() do
        local obj = GetEnemyHeroes(){i}
        if IsValid(obj) and GetDistanceSquared(obj.pos, position) < range * range then
            count = count + 1
        end
    end

    return count
end




local function ShouldWait()
    return myHero.dead or Game.IsChatOpen() or (ExtLibEvade and ExtLibEvade.Evading) or (JustEvade and JustEvade.Evading())
end



class "_Predication"

function _Predication:GetPrediction(source,unit,data)
    if coreMenu.prediction:Value() == 1 then
        local result = GetGamsteronPrediction(unit,data.GamsteronPrediction,source)
        if result and result.CastPosition then
            if result.Hitchance >= data.GamsteronPrediction.HitChance then
                return {
                    castPosition = result.CastPosition,
                    unitPosition = result.UnitPosition
                }
            end
        end
    end

    if coreMenu.prediction:Value() == 2 then
        local result = PremiumPrediction:GetPrediction(source,unit,data.PremiumPrediction)
        if result and result.CastPos then
            if result.HitChance >= data.PremiumPrediction.hitChance then
                return {
                    castPosition = result.CastPos,
                    unitPosition = result.PredPos
                }
            end
        end
    end

    if coreMenu.prediction:Value() == 3 then
        data.GGPrediction.main:GetPrediction(unit, source)
        if data.GGPrediction.main:CanHit(data.GGPrediction.hitChance) then
            return {
                castPosition = data.GGPrediction.main.CastPosition,
                unitPosition = Vector(data.GGPrediction.main.UnitPosition)
            }
        end
    end
end


class  "_Orbwalker"

function _Orbwalker:__init()
    if _G.SDK then
        self.selection = 1
        self.orbwalker = _G.SDK.Orbwalker
        self.targetSelector = _G.SDK.TargetSelector
    end

    if _G.PremiumOrbwalker then
        self.selection = 2
        self.orbwalker = _G.PremiumOrbwalker
    end

end

function _Orbwalker:CanMove()
    if self.selection == 1 then
        return self.orbwalker:CanMove()
    end

    if self.selection == 2 then
        return self.orbwalker:CanMove()
    end
end

function _Orbwalker:CanAttack()
    if self.selection == 1 then
        return self.orbwalker:CanAttack()
    end

    if self.selection == 2 then
        return self.orbwalker:CanAttack()
    end
end

function _Orbwalker:OnPreMovement(func)
    if self.selection == 1 then
        self.orbwalker:OnPreMovement(func)
    end

    if self.selection == 2 then
        self.orbwalker:OnPreMovement(func)
    end
end

function _Orbwalker:OnPreAttack(func)
    if self.selection == 1 then
        self.orbwalker:OnPreAttack(func)
    end

    if self.selection == 2 then
        self.orbwalker:OnPreAttack(func)
    end
end

function _Orbwalker:OnPostAttack(func)
    if self.selection == 1 then
        self.orbwalker:OnPostAttackTick(func)
    end

    if self.selection == 2 then
        self.orbwalker:OnPostAttack(func)
    end
end

function _Orbwalker:SetAttack(bool)
    if self.selection == 1 then
        self.orbwalker:SetAttack(bool)
    end

    if self.selection == 2 then
        self.orbwalker:SetAttack(bool)
    end
end

function _Orbwalker:SetMovement(bool)
    if self.selection == 1 then
        self.orbwalker:SetMovement(bool)
    end

    if self.selection == 2 then
        self.orbwalker:SetMovement(bool)
    end
end

function _Orbwalker:GetMode()
    if self.selection == 1 then
        return self.orbwalker.Modes[0] and "Combo"
        or self.orbwalker.Modes[1] and "Harass"
        or self.orbwalker.Modes[2] and "LaneClear"
        or self.orbwalker.Modes[3] and "LaneClear"
        or self.orbwalker.Modes[4] and "LastHit"
        or self.orbwalker.Modes[5] and "Flee"

    end

    if self.selection == 2 then
       return self.orbwalker:GetMode()
    end

end

function _Orbwalker:GetTarget(range)
    if self.selection == 1 then
        return self.targetSelector:GetTarget(range)
    end
    if self.selection == 2 then
        return self.orbwalker:GetTarget(range)
    end
end














class "Brand"

function Brand:__init()
    self.Q = {
        GamsteronPrediction = {
            HitChance = 3,
            Type = _G.SPELLTYPE_LINE,
            Delay = 0.25,
            Radius = 60,
            Range = 1050,
            Speed = 1600,
            Collision = true,
            MaxCollision = 0,
            CollisionTypes = {_G.COLLISION_ENEMYHERO,_G.COLLISION_MINION, _G.COLLISION_YASUOWALL},
            UseBoundingRadius = true
        },

        PremiumPrediction = {
            hitChance = 0.5,
            speed = 1600,
            range = 1050,
            delay = 0.25,
            radius = 60,
            collision = {"minion","hero","windwall"},
            type = "linear"
        },
        GGPrediction = {
            main = GGPrediction:SpellPrediction({
                Type = GGPrediction.SPELLTYPE_LINE,
                Delay = 0.25,
                Radius = 60,
                Range = 1050,
                Speed = 1600,
                Collision = true,
                MaxCollision = 0,
                CollisionTypes = {GGPrediction.COLLISION_MINION,GGPrediction.COLLISION_ENEMYHERO,GGPrediction.COLLISION_YASUOWALL},
                UseBoundingRadius = true
            }),
            hitChance = 3
        }
    }

    self.W = {
        GamsteronPrediction = {
            HitChance = 3,
            Type = _G.SPELLTYPE_CIRCLE,
            Delay = 0.9,
            Radius = 240,
            Range = 900,
            Speed = math.huge,
            Collision = false,
            UseBoundingRadius = false
        },

        PremiumPrediction = {
            hitChance = 0.5,
            speed = math.huge,
            range = 900,
            delay = 0.9,
            radius = 240,
            collision = nil,
            type = "circular"
        },
        GGPrediction = {
            main = GGPrediction:SpellPrediction({
                Type = GGPrediction.SPELLTYPE_CIRCLE,
                Delay = 0.9,
                Radius = 240,
                Range = 900,
                Speed = math.huge,
                Collision = false,
                -- MaxCollision = 0,
                -- CollisionTypes = {GGPrediction.COLLISION_MINION,GGPrediction.COLLISION_ENEMYHERO,GGPrediction.COLLISION_YASUOWALL},
                UseBoundingRadius = false
            }),
            hitChance = 3
        }

    }

    self.E = {range = 625}
    self.R = {range = 750}

    self.lastQ = 0
    self.lastW = 0
    self.lastE = 0
    self.lastR = 0

    self:LoadMenu()

    Callback.Add("Tick", function() self:Tick() end)
    Callback.Add("Draw", function() self:Draw() end)

end

function Brand:LoadMenu()
    self.tyMenu = MenuElement({type = MENU, id = "14SeriesBrand", name = "[14Series] Brand"})

    self.tyMenu:MenuElement({type = MENU, id = "combo", name = "Combo"})
        self.tyMenu.combo:MenuElement({id = "W", name = "use W", value = true})
        self.tyMenu.combo:MenuElement({id = "EQ", name = "use EQ", value = true})
        self.tyMenu.combo:MenuElement({id = "R", name = "use R", value = true})
        self.tyMenu.combo:MenuElement({id = "RCount", name = "Min R target", value = 2, min = 1, max = 5, step = 1})
        
    self.tyMenu:MenuElement({type = MENU, id = "harass", name = "Harass"})
        self.tyMenu.harass:MenuElement({id = "W", name = "use W", value = true})

    self.tyMenu:MenuElement({type = MENU, id = "auto", name = "Auto"})
        self.tyMenu.auto:MenuElement({id = "Q", name = "auto Q to stun", value = true})
        self.tyMenu.auto:MenuElement({id = "E", name = "auto E on 2 Buff stack", value = true})
        self.tyMenu.auto:MenuElement({type = MENU, id = "antiDash", name = "EQ Anti Dash Target"})
        OnEnemyHeroLoad(function(hero) self.tyMenu.auto.antiDash:MenuElement({id = hero.networkID, name = hero.charName, value = true}) end)

    self.tyMenu:MenuElement({type = MENU, id = "pred", name = "Prediction Setting"})
        self.tyMenu.pred:MenuElement({name = " ", drop = {"Q Prediction"}})
        self.tyMenu.pred:MenuElement({id = "Qgam", name = "Gam Pred HitChance", value = 3, min = 2, max = 4, step = 1, callback = function(value)
            self.Q.GamsteronPrediction.HitChance = value
            print("changed Q Gam Pred HitChance to "..value)
        end})
        self.tyMenu.pred:MenuElement({id = "Qperm", name = "Premium Pred HitChance", value = 0.5, min = 0, max = 1, step = 0.01, callback = function(value)
            self.Q.PremiumPrediction.hitChance = value
            print("changed Q Premium Pred HitChance to "..value)
        end})
        self.tyMenu.pred:MenuElement({id = "QGG", name = "GG Pred HitChance", value = 3, min = 2, max = 4, step = 1, callback = function(value)
            self.Q.GGPrediction.hitChance = value
            print("changed Q GG Pred HitChance to "..value)
        end})

        self.tyMenu.pred:MenuElement({name = " ", drop = {"W Prediction"}})
        self.tyMenu.pred:MenuElement({id = "Wgam", name = "Gam Pred HitChance", value = 3, min = 2, max = 4, step = 1, callback = function(value)
            self.W.GamsteronPrediction.HitChance = value
            print("changed W Gam Pred HitChance to "..value)
        end})
        self.tyMenu.pred:MenuElement({id = "Wperm", name = "Premium Pred HitChance", value = 0.5, min = 0, max = 1, step = 0.01, callback = function(value)
            self.W.PremiumPrediction.hitChance = value
            print("changed W Premium Pred HitChance to "..value)
        end})
        self.tyMenu.pred:MenuElement({id = "WGG", name = "GG Pred HitChance", value = 3, min = 2, max = 4, step = 1, callback = function(value)
            self.W.GGPrediction.hitChance = value
            print("changed W GG Pred HitChance to "..value)
        end})

    self.tyMenu:MenuElement({type = MENU, id = "Drawing", name = "Drawing"})
        self.tyMenu.Drawing:MenuElement({id = "Q", name = "Draw [Q] Range", value = true})
        self.tyMenu.Drawing:MenuElement({id = "W", name = "Draw [W] Range", value = true})
        self.tyMenu.Drawing:MenuElement({id = "E", name = "Draw [E] Range", value = true})
        self.tyMenu.Drawing:MenuElement({id = "R", name = "Draw [R] Range", value = true})

end

-- local predtemp = nil
function Brand:Draw()

    -- if predtemp then
    --     Draw.Circle(predtemp.castPosition, 240 ,Draw.Color(255 ,0xE1,0xA0,0x00))
    --     Draw.Circle(predtemp.unitPosition, 10 ,Draw.Color(255 ,0xE1,0xA0,0x00))

    -- end

    if myHero.health == 0 then return  end

    if self.tyMenu.Drawing.Q:Value() then
        Draw.Circle(myHero.pos, 1050,Draw.Color(255 ,0xE1,0xA0,0x00))
    end

    if self.tyMenu.Drawing.W:Value() then
        Draw.Circle(myHero.pos, 900,Draw.Color(255 ,0xE1,0xA0,0x00))
    end

    if self.tyMenu.Drawing.E:Value() then
        Draw.Circle(myHero.pos, 625,Draw.Color(255 ,0xE1,0xA0,0x00))
    end
    if self.tyMenu.Drawing.R:Value() then
        Draw.Circle(myHero.pos, 750,Draw.Color(255 ,0xE1,0xA0,0x00))
    end
end

function Brand:Tick()
    if ShouldWait() then return end
    if orbwalker:GetMode() == "Combo" then
        self:Combo()
    end

    if orbwalker:GetMode() == "Harass" then
        self:Harass()
    end

    self:Auto()

end

function Brand:Combo()
    local target = orbwalker:GetTarget(625)
    if target and self.tyMenu.combo.EQ:Value() 
    and Ready(_Q) and Ready(_E) 
    and self.lastE + 500 < GetTickCount() then
        local result = prediction:GetPrediction(myHero,target,self.Q)
        if result and result.castPosition then
            if Control.CastSpell(HK_E, target) then
                self.lastE = GetTickCount()
            end
        end
    end

    target = orbwalker:GetTarget(900)
    if target and self.tyMenu.combo.W:Value() then
        self:CastW(target)
    end


    if self.tyMenu.combo.R:Value() and Ready(_R) and self.lastR  + 1000 < GetTickCount() then
        local enemys = GetEnemyHeroes()
        for i = 1 , #enemys do
            local enemy = enemys[i]
            if IsValid(enemy) and GetDistanceSquared(enemy.pos,myHero.pos) < 750 * 750 then
                if GetEnemyCountInRange(480,enemy.pos) >= self.tyMenu.combo.RCount:Value() then
                    if Control.CastSpell(HK_R, enemy) then
                        self.lastR = GetTickCount()  
                    end  
                end
            end
        end
    end

end

function Brand:Harass()
    local target = orbwalker:GetTarget(900)
    if target and self.tyMenu.harass.W:Value() then
        self:CastW(target)
    end
end

function Brand:Auto()
    local enemys = GetEnemyHeroes()
    for i = 1 , #enemys do
        local enemy = enemys[i]

        if IsValid(enemy) then
            local distanceSqr = GetDistanceSquared(myHero.pos, enemy.pos)
            if Ready(_Q) and self.tyMenu.auto.Q:Value() and self.lastQ + 500 < GetTickCount() then
                local buff = Hasbuff(enemy, "BrandAblaze")
                if buff then
                    local result = prediction:GetPrediction(myHero,enemy,self.Q)
                    if result and result.castPosition then
                        local time = 0.25 + math.sqrt(GetDistanceSquared(myHero.pos,result.castPosition) / (1600*1600))
                        if buff.duration > time then
                            if Control.CastSpell(HK_Q, result.castPosition) then
                                self.lastQ = GetTickCount()
                            end
                        end
                    end
                end
            end

            if Ready(_E) and self.tyMenu.auto.E:Value() 
            and self.lastE + 500 < GetTickCount() and distanceSqr < 625*625 then
                local buff = Hasbuff(enemy, "BrandAblaze")
                if buff and buff.count ==2 and buff.duration > 0.25 then
                    if Control.CastSpell(HK_E, enemy) then
                        self.lastE = GetTickCount()
                    end
                end
            end

            if self.tyMenu.auto.antiDash[enemy.networkID] 
            and self.tyMenu.auto.antiDash[enemy.networkID]:Value() 
            and enemy.pathing.isDashing and enemy.pathing.dashSpeed>0 
            and Ready(_Q) and Ready(_E) 
            and self.lastE+350 < GetTickCount() and distanceSqr < 625*625 then
                if Control.CastSpell(HK_E, enemy) then
                    self.lastE = GetTickCount()
                end
            end
        end
    end
end

function Brand:CastW(target)
    if Ready(_W) and self.lastW + 600 < GetTickCount() then
        local result = prediction:GetPrediction(myHero,target,self.W)
        if result and result.castPosition then
            -- predtemp = result
            if Control.CastSpell(HK_W, result.castPosition) then
                self.lastW = GetTickCount()
            end
        end
    end
end

class "FiddleSticks"

function FiddleSticks:__init()
    self.Q = {
        range = 570
    }
    self.W = {
        range = 650
    }

    self.E = {
        GamsteronPrediction = {
            HitChance = 3,
            Type = _G.SPELLTYPE_CIRCLE,
            Delay = 0.4,
            Radius = 120,
            Range = 850,
            Speed = math.huge,
            Collision = false,
            -- MaxCollision = 0,
            -- CollisionTypes = {_G.COLLISION_ENEMYHERO,_G.COLLISION_MINION, _G.COLLISION_YASUOWALL},
            UseBoundingRadius = false
        },

        PremiumPrediction = {
            hitChance = 0.5,
            speed = math.huge,
            range = 850,
            delay = 0.4,
            radius = 120,
            -- collision = {"minion","hero","windwall"},
            type = "circular"
        },

        GGPrediction = {
            main = GGPrediction:SpellPrediction({
                Type = GGPrediction.SPELLTYPE_CIRCLE,
                Delay = 0.4,
                Radius = 120,
                Range = 850,
                Speed = math.huge,
                Collision = false,
                -- MaxCollision = 0,
                -- CollisionTypes = {GGPrediction.COLLISION_YASUOWALL},
                UseBoundingRadius = false
            }),
            hitChance = 2
        }
    }

    self.R = {
        range = 780
    }

    self.lastQ = 0
    self.lastW = 0
    self.lastE = 0
    self.lastR = 0

    self:LoadMenu()

    Callback.Add("Tick", function() self:Tick() end)
    Callback.Add("Draw", function() self:Draw() end)

end

function FiddleSticks:LoadMenu()
    self.tyMenu = MenuElement({type = MENU, id = "14SeriesFiddleSticks", name = "[14Series] FiddleSticks"})

    self.tyMenu:MenuElement({type = MENU, id = "combo", name = "Combo"})
    self.tyMenu.combo:MenuElement({id = "Q", name = "use Q", value = true})
    self.tyMenu.combo:MenuElement({id = "W", name = "use W", value = true})
    self.tyMenu.combo:MenuElement({id = "E", name = "use E", value = true})

    self.tyMenu:MenuElement({type = MENU, id = "harass", name = "Harass"})
    self.tyMenu.harass:MenuElement({id = "E", name = "use E", value = true})

    self.tyMenu:MenuElement({type = MENU, id = "auto", name = "Auto"})
    self.tyMenu.auto:MenuElement({type = MENU, id = "antiDash", name = "Q Anti Dash Target"})
    OnEnemyHeroLoad(function(hero) self.tyMenu.auto.antiDash:MenuElement({id = hero.networkID, name = hero.charName, value = true}) end)

    self.tyMenu:MenuElement({type = MENU, id = "pred", name = "Prediction Setting"})
    self.tyMenu.pred:MenuElement({name = " ", drop = {"E Prediction"}})
    self.tyMenu.pred:MenuElement({id = "Egam", name = "Gam Pred HitChance", value = 3, min = 2, max = 4, step = 1, callback = function(value)
        self.E.GamsteronPrediction.HitChance = value
        print("changed E Gam Pred HitChance to "..value)
    end})
    self.tyMenu.pred:MenuElement({id = "Eperm", name = "Premium Pred HitChance", value = 0.5, min = 0, max = 1, step = 0.01, callback = function(value)
        self.E.PremiumPrediction.hitChance = value
        print("changed E Premium Pred HitChance to "..value)
    end})
    self.tyMenu.pred:MenuElement({id = "EGG", name = "GG Pred HitChance", value = 3, min = 2, max = 4, step = 1, callback = function(value)
        self.E.GGPrediction.hitChance = value
        print("changed E GG Pred HitChance to "..value)
    end})

    self.tyMenu:MenuElement({type = MENU, id = "Drawing", name = "Drawing"})
    self.tyMenu.Drawing:MenuElement({id = "Q", name = "Draw [Q] Range", value = true})
    self.tyMenu.Drawing:MenuElement({id = "W", name = "Draw [W] Range", value = false})
    self.tyMenu.Drawing:MenuElement({id = "E", name = "Draw [E] Range", value = true})
    self.tyMenu.Drawing:MenuElement({id = "R", name = "Draw [R] Range", value = false})

end

function FiddleSticks:Draw()
    if myHero.health == 0 then return  end

    if self.tyMenu.Drawing.Q:Value()  then
        Draw.Circle(myHero.pos, 570,Draw.Color(255 ,0xE1,0xA0,0x00))
    end

    if self.tyMenu.Drawing.W:Value()  then
        Draw.Circle(myHero.pos, 650,Draw.Color(255 ,0xE1,0xA0,0x00))
    end

    if self.tyMenu.Drawing.E:Value() then
        Draw.Circle(myHero.pos, 850,Draw.Color(255 ,0xE1,0xA0,0x00))
    end
    if self.tyMenu.Drawing.R:Value() then
        Draw.Circle(myHero.pos, 780,Draw.Color(255 ,0xE1,0xA0,0x00))
    end
end

function FiddleSticks:Tick()
    if ShouldWait() then return end

    self:Auto()
    
    if myHero.activeSpell.valid and myHero.activeSpell.name == "FiddleSticksW" then
        orbwalker:SetAttack(false)
        orbwalker:SetMovement(false)
        return
    else
        orbwalker:SetAttack(true)
        orbwalker:SetMovement(true)
    end
    

    if orbwalker:GetMode() == "Combo" then
        self:Combo()
    end

    if orbwalker:GetMode() == "Harass" then
        self:Harass()
    end


end

function FiddleSticks:Combo()
    local target = orbwalker:GetTarget(850)
    if target and self.tyMenu.combo.E:Value() then
        self:CastE(target)
    end

    local target = orbwalker:GetTarget(570)
    if target and self.tyMenu.combo.Q:Value() then
        if Ready(_Q) and self.lastQ + 600 < GetTickCount() then
            if Control.CastSpell(HK_Q, target.pos) then
                self.lastQ = GetTickCount()
            end
        end
    end

    local target = orbwalker:GetTarget(650)
    if target and self.tyMenu.combo.W:Value() then
        if Ready(_W) and not Ready(_Q) and self.lastW + 600 < GetTickCount() then
            if Control.CastSpell(HK_W) then
                self.lastW = GetTickCount()
            end
        end
    end

end

function FiddleSticks:Harass()
    local target = orbwalker:GetTarget(850)
    if target and self.tyMenu.harass.E:Value() then
        self:CastE(target)
    end
end

function FiddleSticks:Auto()
    local enemys = GetEnemyHeroes()
    for i = 1 , #enemys do
        local enemy = enemys[i]

        if IsValid(enemy) then
            if self.tyMenu.auto.antiDash[enemy.networkID] 
            and self.tyMenu.auto.antiDash[enemy.networkID]:Value() 
            and enemy.pathing.isDashing and enemy.pathing.dashSpeed>0 
            and Ready(_Q) and self.lastQ + 600 < GetTickCount()
            and GetDistanceSquared(myHero.pos, enemy.pos) < 570 * 570 then
                if Control.CastSpell(HK_Q, enemy) then
                    self.lastQ = GetTickCount()
                end
            end
        end
    end
end

function FiddleSticks:CastE(target)
    if Ready(_E) and self.lastE + 600 < GetTickCount() then
        local result = prediction:GetPrediction(myHero,target,self.E)
        if result and result.castPosition then
            if Control.CastSpell(HK_E, result.castPosition) then
                self.lastE = GetTickCount()
            end
        end
    end
end

class "Vayne"

function Vayne:__init()
    require "2DGeometry"
    require 'MapPositionGOS'

    self.Q = {range = 300, speed = 830}
    self.E = {
        GamsteronPrediction = {
            HitChance = 2,
            Type = _G.SPELLTYPE_LINE,
            Delay = 0.25,
            Radius = 60,
            Range = 650,
            Speed = 220,
            Collision = true,
            MaxCollision = 0,
            CollisionTypes = {_G.COLLISION_YASUOWALL},
            UseBoundingRadius = true
        },

        PremiumPrediction = {
            hitChance = 0.25,
            speed = 2200,
            range = 650,
            delay = 0.25,
            radius = 60,
            collision = {"windwall"},
            type = "linear"
        },
        GGPrediction = {
            main = GGPrediction:SpellPrediction({
                Type = GGPrediction.SPELLTYPE_LINE,
                Delay = 0.25,
                Radius = 60,
                Range = 650,
                Speed = 2200,
                Collision = true,
                MaxCollision = 0,
                CollisionTypes = {GGPrediction.COLLISION_YASUOWALL}
            }),
            hitChance = 2
        }
    }

    self.lastQ = 0
    self.lastW = 0
    self.lastE = 0
    self.lastR = 0

    self.attackTarget = nil
    self:LoadMenu()

    Callback.Add("Tick", function() self:Tick() end)
    Callback.Add("Draw", function() self:Draw() end)
    orbwalker:OnPreAttack(function(...) self:OnPreAttack(...) end)
    orbwalker:OnPostAttack(function() self:OnPostAttack() end)
end

function Vayne:LoadMenu()
    self.tyMenu = MenuElement({type = MENU, id = "14SeriesVayne", name = "[14Series] Vayne"})

    self.tyMenu:MenuElement({type = MENU, id = "combo", name = "Combo"})
            self.tyMenu.combo:MenuElement({id = "Q", name = "Q AA reset", value = true})
            self.tyMenu.combo:MenuElement({id = "QToE", name = "Q To E position", value = true})
            self.tyMenu.combo:MenuElement({id = "E", name = "E", value = true})
            self.tyMenu.combo:MenuElement({id = "blockRAA", name = "Block AA if self invisible", value = false})

        self.tyMenu:MenuElement({type = MENU, id = "harass", name = "Harass"})
            self.tyMenu.harass:MenuElement({id = "Q", name = "Q AA reset", value = true})
            self.tyMenu.harass:MenuElement({id = "EStun", name = "use E stun", value = true})
            self.tyMenu.harass:MenuElement({id = "EStack", name = "use E If Target Have 2 W Stacks", value = true})

        self.tyMenu:MenuElement({type = MENU, id = "waveClear", name = "Wave Clear"})
            self.tyMenu.waveClear:MenuElement({id = "Q", name = "Q AA reset", value = false})


    self.tyMenu:MenuElement({type = MENU, id = "auto", name = "Auto"})
        self.tyMenu.auto:MenuElement({id = "antiMelee", name = "[E] Anti-Melee", value = false})
        self.tyMenu.auto:MenuElement({type = MENU, id = "antiDash", name = "EQ Anti Dash Target"})
        OnEnemyHeroLoad(function(hero) self.tyMenu.auto.antiDash:MenuElement({id = hero.networkID, name = hero.charName, value = true}) end)
    
    self.tyMenu:MenuElement({type = MENU, id = "misc", name = "Misc"})
        self.tyMenu.misc:MenuElement({id = "Qmode", name ="Q mode" , drop = {"To Side", "To Cursor"}})
        self.tyMenu.misc:MenuElement({id = "Erange", name = "E range", value = 400, min = 1, max = 475, step = 1})


    self.tyMenu:MenuElement({type = MENU, id = "pred", name = "Prediction Setting"})
        self.tyMenu.pred:MenuElement({name = " ", drop = {"E Prediction"}})
        self.tyMenu.pred:MenuElement({id = "Egam", name = "Gam Pred HitChance", value = 3, min = 2, max = 4, step = 1, callback = function(value)
            self.E.GamsteronPrediction.HitChance = value
            print("changed E Gam Pred HitChance to "..value)
        end})
        self.tyMenu.pred:MenuElement({id = "Eperm", name = "Premium Pred HitChance", value = 0.5, min = 0, max = 1, step = 0.01, callback = function(value)
            self.E.PremiumPrediction.hitChance = value
            print("changed E Premium Pred HitChance to "..value)
        end})
        self.tyMenu.pred:MenuElement({id = "EGG", name = "GG Pred HitChance", value = 2, min = 2, max = 4, step = 1, callback = function(value)
            self.E.GGPrediction.hitChance = value
            print("changed E GG Pred HitChance to "..value)
        end})

    self.tyMenu:MenuElement({type = MENU, id = "Drawing", name = "Drawing"})
        self.tyMenu.Drawing:MenuElement({id = "Q", name = "Draw [Q] Range", value = true})
        self.tyMenu.Drawing:MenuElement({id = "E", name = "Draw [E] Range", value = true})

end

function Vayne:Draw()
    if myHero.health == 0 then return  end

    if self.tyMenu.Drawing.Q:Value()  then
        Draw.Circle(myHero.pos, 300,Draw.Color(255 ,0xE1,0xA0,0x00))
    end

    if self.tyMenu.Drawing.E:Value()  then
        Draw.Circle(myHero.pos, 650,Draw.Color(255 ,0xE1,0xA0,0x00))
    end

end

function Vayne:Tick()
    if ShouldWait() then return end

    if orbwalker:GetMode() == "Combo" then
        self:Combo()
    end

    if orbwalker:GetMode() == "Harass" then
        self:Harass()
    end

    local ok, err = pcall(function()
        self:Auto()
    end)
    if not ok then
        print(err)
    end

end

function Vayne:OnPreAttack(args)
    if args.Process then
        self.attackTarget = args.Target
    end
end

function Vayne:OnPostAttack()
    if self.attackTarget then
        if Ready(_Q) and self.lastQ + 500 < GetTickCount() and orbwalker:CanMove() then
            if orbwalker:GetMode() == "Combo" and self.tyMenu.combo.Q:Value()
            or orbwalker:GetMode() == "Harass" and self.tyMenu.harass.Q:Value() and self.attackTarget.type == myHero.type
            or orbwalker:GetMode() == "LaneClear" and self.tyMenu.waveClear.Q:Value()
            then
                
                if self.tyMenu.misc.Qmode:Value() == 2 then
                    if Control.CastSpell(HK_Q) then
                        self.lastQ = GetTickCount()
                    end
                end

                if self.tyMenu.misc.Qmode:Value() == 1 then
                    local root1, root2 = CircleCircleIntersection(myHero.pos, self.attackTarget.pos, myHero.range , myHero.range-100)
                    if root1 and root2 then
                        local closest = GetDistanceSquared(root1, mousePos) < GetDistanceSquared(root2, mousePos) and root1 or root2            
                        if Control.CastSpell(HK_Q,myHero.pos:Extended(closest, 300)) then
                            self.lastQ = GetTickCount()
                        end
                    end   
                end

            end
        end
    end
end

function Vayne:Combo()
    local target = orbwalker:GetTarget(650)
    if target and self.tyMenu.combo.E:Value() then
        self:CastEStun(target)
    end

    if self.tyMenu.combo.blockRAA:Value() then
        if Hasbuff(myHero, "vaynetumblefade") then
            orbwalker:SetAttack(false)
        else
            orbwalker:SetAttack(true)
        end
    end

    local target = orbwalker:GetTarget(900)
    if target and self.tyMenu.combo.QToE:Value() then
        if Ready(_Q) and Ready(_E) and self.lastQ + 500 < GetTickCount() and orbwalker:CanMove() then

            local startPos = myHero.pos
            local endPos =  myHero.pos:Extended(Game.mousePos(), 300) 

            for angle = 0, 360, 10 do
                local point = Rotate2D(startPos,endPos, math.rad(angle))
                if GetDistance(point,target.pos) < 650 -100 then
                    if not MapPosition:intersectsWall(LineSegment(point,myHero.pos)) then
                        local extendPos = target.pos:Extended(point,-self.tyMenu.misc.Erange:Value())

                        local lineE = LineSegment(point,extendPos)
                        local angle = AngleBetween(myHero.pos,target.pos, point)

                        if angle <160 and MapPosition:intersectsWall(lineE)  then
                            if Control.CastSpell(HK_Q,point) then
                                self.lastQ = GetTickCount()
                            end
                        end
                    end
                end
            end
        end
    end


end

function Vayne:Harass()
    local target = orbwalker:GetTarget(650)
    if target and self.tyMenu.harass.EStun:Value() then
        self:CastEStun(target)
    end

    if target and self.tyMenu.harass.EStack:Value() then
        if Ready(_E) and self.lastE + 500 < GetTickCount() and orbwalker:CanMove() then
            local buff = Hasbuff(target, "VayneSilveredDebuff")
            if buff and buff.count == 2 then
                if Control.CastSpell(HK_E,target) then
                    self.lastE = GetTickCount()
                end
            end
        end
    end
end

function Vayne:CastEStun(target)
    if Ready(_E) and self.lastE + 500 < GetTickCount() then
        local result = prediction:GetPrediction(myHero,target,self.E)
        if result and result.unitPosition then
            local extendPos = result.unitPosition:Extended(myHero.pos,-self.tyMenu.misc.Erange:Value())
            local lineE = LineSegment(result.unitPosition,extendPos)
            if MapPosition:intersectsWall(lineE) then
                if Control.CastSpell(HK_E,target) then
                    self.lastE = GetTickCount()
                end
            end
        end
    end
end

function Vayne:Auto()
    if Ready(_E) and self.lastE + 500 < GetTickCount() then

        local enemys = GetEnemyHeroes()
        for i = 1 , #enemys do
            local enemy = enemys[i]
            if IsValid(enemy) then
                if self.tyMenu.auto.antiDash[enemy.networkID] 
                and self.tyMenu.auto.antiDash[enemy.networkID]:Value() 
                and enemy.pathing.isDashing and enemy.pathing.dashSpeed>0 
                and GetDistanceSquared(myHero.pos, enemy.pos) < 650 * 650 then
                    if Control.CastSpell(HK_E, enemy) then
                        self.lastE = GetTickCount()
                        return
                    end
                end

                if self.tyMenu.auto.antiMelee:Value() and orbwalker:CanMove() then
                    if enemy.range < 300 and GetDistanceSquared(myHero.pos, enemy.pos) < 300*300 then
                        if Control.CastSpell(HK_E, enemy) then
                            self.lastE = GetTickCount()
                            return
                        end
                    end
                end
            end
        end
    end
end






class "_Loader"

function _Loader:__init()

    self:LoadChmapions()

end

function _Loader:LoadCore()

    orbwalker = _Orbwalker()
    prediction = _Predication()

    self.lastAttack = 0
    self.lastMove = 0

    local pred = {
        -- [1] = function() require 'GamsteronPrediction' end,
        [2] = function() require 'PremiumPrediction' end,
        [3] = function() require 'GGPrediction' end
    }

    local function OnPredChange(val)
        pred[val]()
        print("If you changed Pred, Please F6*2 ")
    end

    coreMenu = MenuElement({type = MENU, id = "14SeriesCore", name = "[14Series] Core"})
    coreMenu:MenuElement({id = "prediction", name = "Prediction To Use", value = 1,drop = {"Gamsteron Pred", "Premium Pred","GG Pred"}, callback = OnPredChange})
    coreMenu:MenuElement({type = MENU, id = "human", name = "Humanizer"})
    coreMenu.human:MenuElement({id = "move", name = "Delay between every move (1000 = 1s)", value = 0, min = 0, max = 500, step = 1})
    coreMenu.human:MenuElement({id = "aa", name = "Delay between every attack (1000 = 1s)", value = 0, min = 0, max = 500, step = 1})

    orbwalker:OnPreAttack(
        function(args)
            if args.Process then
                if self.lastAttack + coreMenu.human.aa:Value() > GetTickCount() then
                    args.Process = false
                    print("block aa")
                else
                    self.lastAttack = GetTickCount()
                end
            end
        end
    )

    orbwalker:OnPreMovement(
        function(args)
            if args.Process then
                if (self.lastMove + coreMenu.human.move:Value() > GetTickCount()) then
                    args.Process = false
                else
                    self.lastMove = GetTickCount()
                end
            end
        end 
    )

end



function _Loader:LoadChmapions()
    local supportChampions = {
        ["Brand"] = true,
        ["FiddleSticks"] = true,
        ["Vayne"] = true,
    }

    if supportChampions[myHero.charName] then
        print("14Series loaded "..myHero.charName)
        self:LoadCore()
        _G[myHero.charName]()
    end
end

function OnLoad()
    if not _G._14Series then
        _G._14Series = true
        _Loader()
    end
end

AutoUpdate()
